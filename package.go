// Package optional provides nullable wrappers for Go primitives.
package optional

//go:generate go run ./genopt/generate.go -o optional.go
